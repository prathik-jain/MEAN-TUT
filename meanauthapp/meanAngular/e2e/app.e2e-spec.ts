import { MeanauthappSrcPage } from './app.po';

describe('meanauthapp-src App', () => {
  let page: MeanauthappSrcPage;

  beforeEach(() => {
    page = new MeanauthappSrcPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
