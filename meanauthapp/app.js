const express = require ('express');
const path = require ('path');
const bodyParser = require ('body-parser');
const cors = require ('cors');
const passport = require('passport');
const mongoose = require ('mongoose');
const config = require ('./config/database');
const users= require ('./routes/users');

// connection to data base
mongoose.Promise=require('bluebird');
mongoose.connect(config.database);

// On connection
mongoose.connection.on('connected', () => {
	console.log('Connected to DataBase'+ config.database);
});

// on error
mongoose.connection.on('error', (err) => {
	console.log('DataBase Error'+ err);
});


const app = express();

const port = process.env.PORT || 8080;


// cors
app.use(cors());


// set static folder
app.use(express.static(path.join(__dirname,'public')));

// body parser middleware
app.use(bodyParser.json());

// passport middleware
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);


// index route
app.get('/', (req, res) => {
	res.send("Invalid Endpoint");
});

app.get('*',(req,res) =>{
	res.sendFile(path.join(__dirname, 'public/index.html'));
});
//User route
app.use('/users',users);

// start server
app.listen(port, () => {
	console.log ('Server started on port ' +port)
});
